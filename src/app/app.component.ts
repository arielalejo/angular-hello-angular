import { Component } from '@angular/core';
import { IFavoriteChangedEventArgs } from './favorite/favorite.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {
    
    posts = {
        id: 1,
        isFavorite :  true
    };

    constructor () {}
    
    handleFavoriteChanged(eventArgs: IFavoriteChangedEventArgs){
        console.log("favorite changed!", eventArgs)
    }
    
}
