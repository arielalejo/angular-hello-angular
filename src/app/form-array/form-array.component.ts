import { Component } from '@angular/core';
import { FormArray, FormGroup, FormControl, AbstractControl } from '@angular/forms';

@Component({
  selector: 'form-array',
  templateUrl: './form-array.component.html',
  styleUrls: ['./form-array.component.css']
})
export class FormArrayComponent {
  courseForm = new FormGroup({
    topics : new FormArray([])
  })

  constructor() { }

  addTopic(topic: HTMLInputElement){
    const topics = this.courseForm.get('topics') as FormArray;
    topics.push(new FormControl(topic.value));

    topic.value='';
  }

  removeTopic(topicIndex: number){
    const topics = this.topics;
    topics.removeAt(topicIndex);

  }

  get topics(): FormArray {return this.courseForm.get('topics') as FormArray}

}
