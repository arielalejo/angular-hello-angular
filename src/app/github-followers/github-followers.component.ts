import { Component, OnInit } from '@angular/core';
import { GithubFollowersService } from '../services/github-followers.service';
import { ActivatedRoute } from '@angular/router';
import { combineLatest } from "rxjs";
import { map, switchMap } from "rxjs/operators";

@Component({
  selector: 'github-followers',
  templateUrl: './github-followers.component.html',
  styleUrls: ['./github-followers.component.css']
})
export class GithubFollowersComponent implements OnInit {
  followers: any[];

  constructor(private githubService: GithubFollowersService<any>, private route: ActivatedRoute) { }

  ngOnInit(): void {
    let obs = combineLatest([this.route.paramMap, this.route.queryParamMap]);

    obs.pipe(
      switchMap(combined=>{
        console.log(combined);

        let id = combined[0].get('id');
        let page = combined[1].get('page');
        
        return this.githubService.getResources() //e.g.: this.service.getAll({id:id, page:page})
      
      }))
      .subscribe( response =>  this.followers = response );

      
  }
}
