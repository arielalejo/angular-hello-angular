import { Component } from '@angular/core';
import { CoursesService } from '../courses.service/courses.service';

@Component({
    selector: 'courses',
    template: `
        <h3>{{getTitle()}}</h3>
        <ul>
            <li *ngFor="let course of courses">
                {{ course}}
            </li>
        </ul>
    `
})
export class CoursesComponent{
    title: string = "List of courses";
    courses: string[];

    constructor(service: CoursesService){
        this.courses = service.getCourses();
    }

    getTitle(): string{
        return this.title;
    }
}