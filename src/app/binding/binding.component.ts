import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-binding',
    templateUrl: './binding.component.html',
    styleUrls: ['./binding.component.css'],
})
export class BindingComponent implements OnInit {
    isActive: boolean = false;
    fontColor: string = 'blue';
    title = 'hello-worlddd';
    name: string = 'Enlil';

    constructor() {}

    ngOnInit(): void {}

    onInputKeyUp(event: any) {
        event.stopPropagation();
        console.log(event.target.value);
    }

    onEnterKeyUp(event: any) {
        event.stopPropagation();
        console.log(event.target.value);
    }

    onInputWithTemplateVariable(email: string) {
        console.log('template variable: ', email);
    }

    onTwoWayBinding() {
        console.log('two way binding: ', this.name);
    }

    onButtonClick() {
        this.isActive = !this.isActive;
    }
}
