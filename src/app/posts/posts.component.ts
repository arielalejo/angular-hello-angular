import { Component, OnInit } from '@angular/core';
import { AppError } from '../common/app-error';
import { BadRequestError } from '../common/bad-request-error';
import { NotFoundError } from '../common/not-found-error';
import { PostsService } from '../services/posts.service';
import { IPost } from "./IPost";

@Component({
    selector: 'posts',
    templateUrl: './posts.component.html',
    styleUrls: ['./posts.component.css'],
})
export class PostsComponent implements OnInit {
    posts: IPost[];

    constructor(private postsService: PostsService<IPost>) {}

    ngOnInit(): void {
        this.postsService.getResources().subscribe(
            (response: IPost[]) => {
                this.posts = response;
                console.log(response)
            },
            (error: AppError) =>{ throw error }
        );
    }

    createPost(title: HTMLInputElement) {
        const post: IPost = {
            body: "dummy body",
            title: title.value
        };

        this.posts.splice(0, 0, post);

        this.postsService.createResource(post).subscribe(
            (response) => {
                post['id'] = response.body?.id;
                console.log(response)
            },
            (error: AppError) => {
                if (error instanceof BadRequestError) {                    
                    this.posts.splice(0,0);
                    console.log(error)
                    // form.setErrors(error)
                }

                else throw error;
            }
        );

        title.value = '';
    }

    deletePost(post: IPost) {
        const postsClone = [...this.posts];

        const id = post.id;
        this.posts.splice(this.posts.indexOf(post), 1);

        this.postsService.deleteResource(id).subscribe(
            (response) => {                                
                console.log(response)
            },
            (error: AppError) => {
                if (error instanceof NotFoundError) {
                    this.posts = postsClone;
                    console.log(error)
                    alert("this post has already deleted")
                }
                else throw error;
            }
        );
    }

    getPost(){
        this.postsService.getResourceById(452).subscribe(
            (response: IPost) => {                
                console.log(response)
            },
            (error: AppError) => {
                if (error instanceof NotFoundError) {
                    console.log(error)
                    alert("Resourse not found")
                }
                else throw error;
            }
        ); 
    }
}
