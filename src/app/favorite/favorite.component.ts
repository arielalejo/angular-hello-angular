import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {
  @Input("is-favorite") isFavorite: boolean;
  @Output() mychange = new EventEmitter();

  constructor() { }

  isIconClicked(){
    this.isFavorite = !this.isFavorite;
    this.mychange.emit(<IFavoriteChangedEventArgs>{value: this.isFavorite});
  }

  ngOnInit(): void {
  }

}

export interface IFavoriteChangedEventArgs{
  value: boolean
}