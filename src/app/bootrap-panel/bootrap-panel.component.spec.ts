import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BootrapPanelComponent } from './bootrap-panel.component';

describe('BootrapPanelComponent', () => {
  let component: BootrapPanelComponent;
  let fixture: ComponentFixture<BootrapPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BootrapPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BootrapPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
