import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsernameValidator } from './username.validators';

@Component({
    selector: 'signup-form',
    templateUrl: './signup-form.component.html',
    styleUrls: ['./signup-form.component.css'],
})
export class SignupFormComponent {
    signUpForm = new FormGroup({
        account: new FormGroup({
            username: new FormControl('', [
                Validators.required,
                Validators.minLength(4),
                UsernameValidator.cannotContainSpace
            ],  
                UsernameValidator.shoulBeUnique
            ),
            password: new FormControl('', Validators.required)
        })
    });
    constructor() {}

    get username () : FormControl {
        return <FormControl>this.signUpForm.get('account.username')
    }
    
    login(){
        // this.signUpForm.setErrors({
        //     someFormError: true
        // })        

        this.signUpForm.reset('')
    }
}
