import { AbstractControl, ValidationErrors } from "@angular/forms";

export class UsernameValidator{
    static cannotContainSpace(control: AbstractControl) : ValidationErrors | null{
        if (control.touched){
            if (( <string>control.value).indexOf(' ') >= 0)
                return { cannotContainSpace: true }
        }
        return null;
    }

    static shoulBeUnique(control: AbstractControl) : Promise<ValidationErrors | null> {
        return new Promise((resolve, reject) => {
            setTimeout(()=> {
                if ((<string>control.value) === "ariel")
                    resolve( { shoulBeUnique: true});
                else 
                    resolve( null);
            }, 2000)
        })
    }
}