import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import {catchError, map, retry} from  'rxjs/operators';
import { AppError } from '../common/app-error';
import { NotFoundError } from '../common/not-found-error';
import { BadRequestError } from '../common/bad-request-error';


export class DataService<T> {   

    constructor(private url: string, private http: HttpClient){
        
    }

    getResources(): Observable<T[]>{
        return this.http.get<T[]>(this.url)
            .pipe(
                catchError(this.handleError)
            )
    }

    createResource(resource: T) : Observable<HttpResponse<T>>{
        return this.http.post<T>(this.url, resource, {observe: 'response'} )
            .pipe(
                catchError(this.handleError)
            )
    }

    deleteResource(id: any){
        return this.http.delete<T>(this.url+'/'+id, {observe: 'response'})
            .pipe(
                catchError(this.handleError)
            )
    }

    getResourceById(id: any): Observable<T>{
        return this.http.get<T>(this.url + '/' + id)
            .pipe(
                retry(3),
                catchError(this.handleError)
            )
    }

    private handleError(error: HttpErrorResponse){
        switch (error.status) {
          case 400:
            return throwError(new BadRequestError(error))
        
          case 404:
            return throwError(new NotFoundError(error))
          
          default:
            return throwError( new AppError(error));
        }
    }
}
