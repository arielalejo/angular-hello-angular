import { Inject, Injectable } from "@angular/core";
import { DataService } from "./data.service";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PostsService<T> extends DataService<T>{
    constructor(http: HttpClient){
        super("https://jsonplaceholder.typicode.com/posts", http)
    }
}