import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseComponent } from './course/course.component';
import { CoursesService } from './courses.service/courses.service';
import { FavoriteComponent } from './favorite/favorite.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BootrapPanelComponent } from './bootrap-panel/bootrap-panel.component';
import { DirectiveComponentComponent } from './directive-component/directive-component.component';
import { InputFormatDirective } from './custom-directives/input-format.directive';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { FormArrayComponent } from './form-array/form-array.component';
import { PostsComponent } from './posts/posts.component';
import { HttpClientModule } from '@angular/common/http';
import { PostsService } from './services/posts.service';
import { BindingComponent } from './binding/binding.component';
import { AppUnexpecErrorHandler } from './common/app-unexp-error-handler';
import { GithubFollowersComponent } from './github-followers/github-followers.component';
import { GithubFollowersService } from './services/github-followers.service';
import { HomeComponent } from './home/home.component';
import { GithubProfileComponent } from './github-profile/github-profile.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    CourseComponent,
    FavoriteComponent,
    BootrapPanelComponent,
    DirectiveComponentComponent,
    InputFormatDirective,
    SignupFormComponent,
    FormArrayComponent,
    PostsComponent,
    BindingComponent,
    GithubFollowersComponent,
    HomeComponent,
    GithubProfileComponent,
    NotFoundComponent,
    NavbarComponent,
  ],
  imports: [    
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    CoursesService,
    PostsService,
    {provide: ErrorHandler, useClass: AppUnexpecErrorHandler},
    GithubFollowersService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
