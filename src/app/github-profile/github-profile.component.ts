import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-github-follower-profile',
  templateUrl: './github-profile.component.html',
  styleUrls: ['./github-profile.component.css']
})
export class GithubProfileComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.paramMap
      .subscribe((params)=>{
        console.log(params)
        console.log(params.get("id"))
      })
  }

  onSubmit(){
    //this.router.navigate(['/followers']) // to simple url
    //this.router.navigate(['/followers', 12, 1900 ]) // with url parameters
    this.router.navigate(['/followers'], {queryParams: {page:1, order:"oldest"}}) // to url with query strings     
  }
}
