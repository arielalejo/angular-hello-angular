import { Component } from '@angular/core';

@Component({
    selector: 'directive-component',
    templateUrl: './directive-component.component.html',
    styleUrls: ['./directive-component.component.css'],
})
export class DirectiveComponentComponent {
    courses: number[] = [1, 2];
    view: string = '';
    users: any;

    constructor() {}

    trackCourse(id:any, course:any){
      return course ? course.id : undefined
    }

    getUsers() {
        this.users = [
            { id: 1, title: 'user 1' },
            { id: 2, title: 'user 2' },
            { id: 3, title: 'user 3' },
        ];
    }

    setView(view: string) {
        this.view = view;
    }

    onAddUser() {
        this.users.push({
            id: this.users.length + 1,
            title: 'new user',
        });
    }

    onRemoveUser(user: any) {
        const index = this.users.indexOf(user);
        this.users.splice(index, 1);
    }

    onChangeUser(user: any) {
        const index = this.users.indexOf(user);
        this.users[index].title = 'changed';
    }
}
